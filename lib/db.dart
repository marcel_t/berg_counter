import 'package:shared_preferences/shared_preferences.dart';

Future<void> dbSet(String groupName, int value) async {
  String dbKey = 'counter_$groupName';

  SharedPreferences prefs = await SharedPreferences.getInstance();

  await prefs.setInt(dbKey, value);
}

Future<int> dbRead(String groupName) async {
  String dbKey = 'counter_$groupName';

  SharedPreferences prefs = await SharedPreferences.getInstance();

  return prefs.getInt(dbKey) ?? 0;
}

Future<Map<String, int>> dbReadAll() async {
  var itnValue = await dbRead('ITN');
  var lmValue = await dbRead('LM');
  var videoValue = await dbRead('Video');
  var fiberValue = await dbRead('Fiber');
  var pdaValue = await dbRead('PDA');
  var spaValue = await dbRead('SPA');
  var nfbValue = await dbRead('NFB');
  // var pdaValue = await dbRead('');
  //...

  return {
    'ITN': itnValue,
    'LM': lmValue,
    'Video': videoValue,
    'Fiber': fiberValue,
    'PDA': pdaValue,
    'SPA': spaValue,
    'NFB': nfbValue,
    //...
  };
}
