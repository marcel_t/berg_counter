import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ListContainer extends StatelessWidget {
  final Color containerColor;
  final String groupText;
  final int currentCount;
  final void Function(int) onDecrementClicked;
  final void Function(int) onIncrementClicked;
  final void Function(String, int)? onEditNumberClicked;

  const ListContainer({
    super.key,
    required this.containerColor,
    required this.groupText,
    required this.currentCount,
    required this.onDecrementClicked,
    required this.onIncrementClicked,
    this.onEditNumberClicked,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: containerColor,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          IconButton(
              icon: const Icon(
                Icons.edit,
              ),
              onPressed: () {
                HapticFeedback.vibrate();

                onEditNumberClicked?.call(groupText, currentCount);
              }),
          IconButton(
              icon: const Icon(
                Icons.remove,
              ),
              onPressed: () {
                HapticFeedback.vibrate();

                onDecrementClicked.call(currentCount);
              }),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Column(
                children: [
                  Text(
                    groupText,
                    style: const TextStyle(
                      fontSize: 20,
                      color: Colors.black,
                    ),
                  ),
                  Text(
                    currentCount.toString(),
                  ),
                ],
              ),
            ),
          ),
          IconButton(
            color: Colors.transparent,
            icon: const Icon(
              Icons.edit,
            ),
            onPressed: () {},
          ),
          IconButton(
            icon: const Icon(
              Icons.add,
            ),
            onPressed: () {
              HapticFeedback.vibrate();
              onIncrementClicked.call(currentCount);
            },
          ),
        ],
      ),
    );
  }
}
