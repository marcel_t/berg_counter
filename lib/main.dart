import 'package:berg_test/dashboard.dart';
import 'package:berg_test/db.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:flutter/services.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  var savedValues = await dbReadAll();

  runApp(
    MaterialApp(
      home: Dashboard(
        currentValues: savedValues,
      ),
    ),
  );
}
