import 'dart:async';

import 'package:berg_test/db.dart';
import 'package:berg_test/export_handler.dart';
import 'package:berg_test/list_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';

const int elementCount = 24;

class Dashboard extends StatefulWidget {
  final Map<String, int> currentValues;
  const Dashboard({
    super.key,
    required this.currentValues,
  });

  @override
  State<StatefulWidget> createState() => DashboardState();
}

class DashboardState extends State<Dashboard> {
  bool showLoadingWidget = false;
  TextEditingController numberInputTextEditController = TextEditingController();

  int? itnValue;
  int? lmValue;
  int? videoValue;
  int? fiberValue;
  int? pdaValue;
  int? spaValue;
  int? nfbValue;
  //...

  @override
  void initState() {
    initValues(widget.currentValues);

    super.initState();
  }

  void initValues(Map<String, int> values) {
    setState(() {
      itnValue = values['ITN'];
      lmValue = values['LM'];
      videoValue = values['Video'];
      fiberValue = values['Fiber'];
      pdaValue = values['PDA'];
      spaValue = values['SPA'];
      nfbValue = values['NFB'];
    });
  }

  Future<void> showFileWriteErrorDialog() async {
    return await showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('File export'),
          content: const Text(
            'Fehler beim export',
          ),
          actions: <Widget>[
            TextButton(
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: const Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> showSendMailDialog(void Function(bool) dialogSelection) async {
    return await showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Mail senden?'),
          actions: <Widget>[
            TextButton(
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: const Text('Ja'),
              onPressed: () {
                Navigator.of(context).pop();
                dialogSelection.call(true);
              },
            ),
            TextButton(
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: const Text('Nein'),
              onPressed: () {
                Navigator.of(context).pop();
                dialogSelection.call(false);
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> sendMail(String attachmentPath) async {
    final Email email = Email(
      body: 'Test body',
      subject: 'Test subject',
      recipients: ['fakemail@fake.com'],
      attachmentPaths: [attachmentPath],
      isHTML: false,
    );

    String platformResponse;

    try {
      await FlutterEmailSender.send(email);
      platformResponse = 'success';
    } catch (error) {
      platformResponse = error.toString();
    }

    if (!mounted) return;

    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(platformResponse),
      ),
    );
  }

  Future<void> _showNumberInputDialog(
    String groupName,
    int currentNumber,
  ) async {
    return await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          title: Text('Aktueller Wert $currentNumber'),
          content: TextField(
            controller: numberInputTextEditController,
            keyboardType: TextInputType.number,
            decoration: const InputDecoration(
              hintText: 'Neuer Wert',
            ),
          ),
          actions: [
            TextButton(
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: const Text('Cancel'),
              onPressed: () {
                numberInputTextEditController.text = '';
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: const Text('OK'),
              onPressed: () async {
                int? newValue =
                    int.tryParse(numberInputTextEditController.text);
                if (newValue != null) {
                  dbSet(groupName, newValue);
                }
                var newDbValues = await dbReadAll();
                initValues(newDbValues);
                numberInputTextEditController.text = '';
                if (mounted) {
                  Navigator.of(context).pop();
                }
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        centerTitle: true,
        title: const Text(
          'Dashboard Anästhesie',
        ),
        actions: [
          IconButton(
            onPressed: () async {
              Completer<bool> completer = Completer<bool>();
              await showSendMailDialog((sendMail) {
                completer.complete(sendMail);
              });

              var proceedWithMail = await completer.future;

              if (!proceedWithMail) {
                return;
              }

              setState(() {
                showLoadingWidget = true;
              });

              await Future.delayed(const Duration(milliseconds: 400));

              var exportHandler = ExportHandler();
              String? filePath = await exportHandler.writeCsvToStorage();

              if (filePath == null) {
                await showFileWriteErrorDialog();
              } else {
                await sendMail(filePath);
              }

              setState(() {
                showLoadingWidget = false;
              });
            },
            icon: const Icon(
              Icons.mail_outline,
            ),
          ),
        ],
      ),
      body: showLoadingWidget
          ? const Center(
              child: CircularProgressIndicator(
                color: Colors.green,
              ),
            )
          : ListView(
              children: [
                ListContainer(
                  containerColor: Colors.greenAccent,
                  groupText: 'ITN',
                  currentCount: itnValue ?? 0,
                  onDecrementClicked: (oldValue) async {
                    int newValue = oldValue -= 1;
                    await dbSet('ITN', newValue);
                    setState(() {
                      itnValue = newValue;
                    });
                  },
                  onIncrementClicked: (oldValue) async {
                    int newValue = oldValue += 1;
                    await dbSet('ITN', newValue);
                    setState(() {
                      itnValue = newValue;
                    });
                  },
                  onEditNumberClicked: (groupSource, currentValue) async {
                    await _showNumberInputDialog(groupSource, currentValue);
                  },
                ),
                const SizedBox(height: 1),
                ListContainer(
                  containerColor: Colors.greenAccent,
                  groupText: 'LM',
                  currentCount: lmValue ?? 0,
                  onDecrementClicked: (oldValue) async {
                    int newValue = oldValue -= 1;
                    await dbSet('LM', newValue);
                    setState(() {
                      lmValue = newValue;
                    });
                  },
                  onIncrementClicked: (oldValue) async {
                    int newValue = oldValue += 1;
                    await dbSet('LM', newValue);
                    setState(() {
                      lmValue = newValue;
                    });
                  },
                  onEditNumberClicked: (groupSource, currentValue) async {
                    await _showNumberInputDialog(groupSource, currentValue);
                  },
                ),
                const SizedBox(height: 1),
                ListContainer(
                  containerColor: Colors.greenAccent,
                  groupText: 'Video',
                  currentCount: videoValue ?? 0,
                  onDecrementClicked: (oldValue) async {
                    int newValue = oldValue -= 1;
                    await dbSet('Video', newValue);
                    setState(() {
                      videoValue = newValue;
                    });
                  },
                  onIncrementClicked: (oldValue) async {
                    int newValue = oldValue += 1;
                    await dbSet('Video', newValue);
                    setState(() {
                      videoValue = newValue;
                    });
                  },
                  onEditNumberClicked: (groupSource, currentValue) async {
                    await _showNumberInputDialog(groupSource, currentValue);
                  },
                ),
                const SizedBox(height: 1),
                ListContainer(
                  containerColor: Colors.greenAccent,
                  groupText: 'Fiber',
                  currentCount: fiberValue ?? 0,
                  onDecrementClicked: (oldValue) async {
                    int newValue = oldValue -= 1;
                    await dbSet('Fiber', newValue);
                    setState(() {
                      fiberValue = newValue;
                    });
                  },
                  onIncrementClicked: (oldValue) async {
                    int newValue = oldValue += 1;
                    await dbSet('Fiber', newValue);
                    setState(() {
                      fiberValue = newValue;
                    });
                  },
                ),
                const SizedBox(height: 1),
                ListContainer(
                  containerColor: Colors.orangeAccent.shade100,
                  groupText: 'PDA',
                  currentCount: pdaValue ?? 0,
                  onDecrementClicked: (oldValue) async {
                    int newValue = oldValue -= 1;
                    await dbSet('PDA', newValue);
                    setState(() {
                      pdaValue = newValue;
                    });
                  },
                  onIncrementClicked: (oldValue) async {
                    int newValue = oldValue += 1;
                    await dbSet('PDA', newValue);
                    setState(() {
                      pdaValue = newValue;
                    });
                  },
                ),
                const SizedBox(height: 1),
                ListContainer(
                  containerColor: Colors.orangeAccent.shade100,
                  groupText: 'SPA',
                  currentCount: spaValue ?? 0,
                  onDecrementClicked: (oldValue) async {
                    int newValue = oldValue -= 1;
                    await dbSet('SPA', newValue);
                    setState(() {
                      spaValue = newValue;
                    });
                  },
                  onIncrementClicked: (oldValue) async {
                    int newValue = oldValue += 1;
                    await dbSet('SPA', newValue);
                    setState(() {
                      spaValue = newValue;
                    });
                  },
                ),
                const SizedBox(height: 1),
                ListContainer(
                  containerColor: Colors.orangeAccent.shade100,
                  groupText: 'NFB',
                  currentCount: nfbValue ?? 0,
                  onDecrementClicked: (oldValue) async {
                    int newValue = oldValue -= 1;
                    await dbSet('NFB', newValue);
                    setState(() {
                      nfbValue = newValue;
                    });
                  },
                  onIncrementClicked: (oldValue) async {
                    int newValue = oldValue += 1;
                    await dbSet('NFB', newValue);
                    setState(() {
                      nfbValue = newValue;
                    });
                  },
                ),
              ],
            ),
    );
  }
}
